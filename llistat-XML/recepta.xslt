<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/receptes/recepta">
        <html>
            <head>
                <title>
                    <xsl:value-of select="titol"/>
                </title>
                <link rel="stylesheet" href="../css/estil.css"/>
            </head>

            <body>
                <header>
                    <h1><img src="../imatges/Logo-Delicias.png"/></h1>
                </header>

                <nav>
                    <a class="menu" href="../index.html">INICI</a>
                    <a class="menu" href="../sobre-mi/sobre-mi.html">SOBRE MI</a>
                    <a class="menu" href="../llistat-XML/receptes-llistat.xml">RECEPTES</a>
                    <a class="menu" href="../contacte/contacte.html">CONTACTE</a>
                    <a class="menu" href="../altres-coses/altres-coses.html">ALTRES COSES</a>
                </nav>

                <main class="recipe">
                    <div class="columna-esq-66">
                            <div class="recepta_full">

                            <div class="header-recepta">
                                <h2 class="titol-recepta"><xsl:value-of select="titol"/></h2>
                                <p class="autor-recepta">Per <b><xsl:value-of select="autor"/></b></p>
                                <p class="data-recepta"><xsl:value-of select="data"/></p>
                                <hr class="border"/>
                                <p class="descripcio">
                                    <xsl:value-of select="descripcio"/>
                                </p>
                                <img class="portada" src="{imatges/imatge[1]/@src}" alt="foto_recepta" width="100%" height="auto"/>
                                <p class="molt-text">
                                    <xsl:value-of select="introduccio"/>
                                </p>
                            </div>

                            <div class="body-recepta">
                                <h2 class="sub-titol">Ingredients</h2>
                                <ul class="ingredients">
                                    <xsl:for-each select="./ingredients//ingredient">
                                        <li>
                                            <xsl:value-of select="."/>
                                        </li>
                                    </xsl:for-each>
                                </ul>

                                <h2 class="sub-titol">Elaboració pas a pas</h2>
                                <ol class="elaboració">
                                    <xsl:for-each select="./elaboracio//pas">
                                        <li><b><xsl:value-of select="nom"/></b><xsl:value-of select="descripcio"/></li>
                                    </xsl:for-each>
                                </ol>
                            </div>
                        </div>
                    </div>
                    
                    <div class="columna-dreta-33">
                        <div class="contenidor-aboutme">
                            <div class="aboutme">
                                <h3><a class="menu" href="../sobre-mi/sobre-mi.html">Sobre mi</a></h3>
                                <img class="foto-nosaltres" src="../imatges/the-best.png" alt="foto_nosaltres" width="100%" height="auto"/>
                                <p class="aboutmetext">Hola, benvingut/da al meu blog, un espai on vull compartir
                                les meves receptes vegetarianes i veganes preferides...</p>
                            </div>
                
                            <div class="subscribe">
                                <h3>Subscriu-te al meu blog</h3>
                                <img src="../imatges/libro.png" alt="Foto de llibre" width="50%" height="auto"/>
                                <p class="aboutmetext">I aconsegueix GRATIS el meu eBook amb 10 receptes exclusives amb superaliments</p>
                                <a class="boto_apuntam" href="../subscriu-te/subscriu-te.html"><p>Apunta'm!</p></a>
                            </div>
                        </div>

                        <div class="publicitat">
                            <a href="https://www.itb.cat"><img src="../imatges/publicitat.png" width="100%" height="auto"/></a>
                        </div>
                    </div>

                </main>

                <footer>
                    <!-- "Anchor tags" para linkear red social etc -->
                    <a class="boto_footer" href="https://es-es.facebook.com/">Facebook</a>
                    <a class="boto_footer" href="https://twitter.com/?lang=ES">Twitter</a>
                    <a class="boto_footer" href="https://www.instagram.com/">Instagram</a>
                    <a class="boto_footer" href="https://www.pinterest.es/">Pinterest</a>
                    <a class="boto_footer" href="https://mail.google.com/mail">Email</a>
                    <a class="boto_footer" href="https://es.wikipedia.org/wiki/RSS">RSS</a>
                </footer>
            </body>

        </html>
    </xsl:template>
</xsl:stylesheet>