<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/receptes">
        <html>
            <head>
                <title>Receptes</title>
                <link rel="stylesheet" href="../css/estil.css"/>
            </head>

            <body>
    <header>
        <h1><img src="../imatges/Logo-Delicias.png"/></h1>
    </header>

    <nav>
        <a class="menu" href="../index.html">INICI</a>
        <a class="menu" href="../sobre-mi/sobre-mi.html">SOBRE MI</a>
        <a class="menu" href="../llistat-XML/receptes-llistat.xml">RECEPTES</a>
        <a class="menu" href="../contacte/contacte.html">CONTACTE</a>
        <a class="menu" href="../altres-coses/altres-coses.html">ALTRES COSES</a>
    </nav>

    <main>

        <div class="llistat">
            <xsl:for-each select="recepta">
                <xsl:sort select="titol"/>
                    <div class="element_llistat">
                    <div class="contenidor_imatge_llistat_descripcio"><a href="receptes-detall.xml"><img class="foto_recepta" src="{imatges/imatge/@src}" alt="foto_recepta" height="170px" width="254px"/></a></div>
                    <div class="llistat_descripcio">
                        <h2><a class="menu" href="receptes-detall.xml"><xsl:value-of select="titol"/></a></h2>
                        <p><xsl:value-of select="descripcio"/></p>
                        <div class="contenidor_caracterisiques">
                            <div class="caracteristica_llistat_descripcio"><p><b>Dificultat: </b><xsl:value-of select="dificultat"/></p></div>
                            <div class="caracteristica_llistat_descripcio"><p><b>Cuina: </b><xsl:value-of select="cuina"/></p></div>
                            <div class="caracteristica_llistat_descripcio"><p><b>Vegetariana: </b><xsl:value-of select="vegetariana"/></p></div>
                            <div class="caracteristica_llistat_descripcio"><p><b>Celíacs: </b><xsl:value-of select="celiacs"/></p></div>
                        </div>
                        <div class="contenidor_logos">
                            <xsl:choose>
                                <xsl:when test="dificultat = 'Baixa'">
                                <div class="logo_llistat"><img src="../icones/logo-easy.png" alt="logo-easy" height="100%"/></div>
                                </xsl:when>
                               <xsl:when test="dificultat = 'Mitja'">
                                <div class="logo_llistat"><img src="../icones/logo-medium.png" alt="logo-medium" height="100%"/></div>
                                </xsl:when>
                                <xsl:when test="dificultat = 'Alta'">
                                <div class="logo_llistat"><img src="../icones/logo-hard.png" alt="logo-easy" height="100%"/></div>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when test="cuina = 'Saludable'">
                                    <div class="logo_llistat"><img src="../icones/logo-healthy-food.png" alt="logo-healthy" height="100%"/></div>
                                </xsl:when>
                                <xsl:when test="cuina = 'Snack'">
                                    <div class="logo_llistat"><img src="../icones/logo-snack.png" alt="logo-snack" height="100%"/></div>
                                </xsl:when>
                                <xsl:when test="cuina = 'Postres'">
                                    <div class="logo_llistat"><img src="../icones/logo-postres.png" alt="logo-postres" height="100%"/></div>
                                </xsl:when>
                                <xsl:when test="cuina = 'Còsmica'">
                                    <div class="logo_llistat"><img src="../icones/logo-hole.jpg" alt="logo-hole" height="100%"/></div>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when test="vegetariana = 'Sí'">
                                    <div class="logo_llistat"><img src="../icones/logo-vegan.png" alt="logo-vegan" height="100%"/></div>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when test="celiacs = 'Sí'">
                                    <div class="logo_llistat"><img src="../icones/logo-celiac.jpg" alt="logo-celiac" height="100%"/></div>
                                </xsl:when>
                            </xsl:choose>
                        </div>
                        <p class="autor"><b>Autor: </b>Delicias Kitchen</p>
                    </div>
                </div>
            </xsl:for-each>
            <!--Paginador no operatiu
                <div class="paginador">
                    <div class="boto_paginador_extrem">
                        <a class="boto_paginador" href="../llistat/llistat.html">
                            <p>prev</p>
                        </a>
                    </div>
                    <div class="boto_paginador_seleccionat">
                        <a class="boto_paginador" href="../llistat/llistat.html">
                            <p>1</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat2.html">
                            <p>2</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat3.html">
                            <p>3</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat4.html">
                            <p>4</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat5.html">
                            <p>5</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat6.html">
                            <p>6</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat7.html">
                            <p>7</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat8.html">
                            <p>8</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat9.html">
                            <p>9</p>
                        </a>
                    </div>
                    <div class="boto_paginador">
                        <a class="boto_paginador" href="../llistat/llistat10.html">
                            <p>10</p>
                        </a>
                    </div>
                    <div class="boto_paginador_extrem">
                        <a class="boto_paginador" href="../llistat/llistat2.html">
                            <p>next</p>
                        </a>
                    </div>
                </div>
            -->
        </div>


    </main>

    <aside>

        <div class="aboutme_subscribe">
            <div class="aboutme">
                <h3><a class="menu" href="../sobre-mi/sobre-mi.html">Sobre mi</a></h3>
                <img class="foto-nosaltres" src="../imatges/the-best.png" alt="foto_nosaltres" width="100%" height="auto"/>
                <p class="aboutmetext">Hola, benvingut/da al meu blog, un espai on vull compartir
                les meves receptes vegetarianes i veganes preferides...</p>
            </div>

            <div class="subscribe">
                <h3>Subscriu-te al meu blog</h3>
                <img src="../imatges/libro.png" alt="Foto de llibre" width="50%" height="auto"/>
                <p class="aboutmetext">I aconsegueix GRATIS el meu eBook amb 10 receptes exclusives amb superaliments</p>
                <a class="boto_apuntam" href="../subscriu-te/subscriu-te.html"><p>Apunta'm!</p></a>
            </div>
        </div>

    </aside>

    <footer>
        <!-- "Anchor tags" para linkear red social etc -->
        <a class="boto_footer" href="https://es-es.facebook.com/">Facebook</a>
        <a class="boto_footer" href="https://twitter.com/?lang=ES">Twitter</a>
        <a class="boto_footer" href="https://www.instagram.com/">Instagram</a>
        <a class="boto_footer" href="https://www.pinterest.es/">Pinterest</a>
        <a class="boto_footer" href="https://mail.google.com/mail">Email</a>
        <a class="boto_footer" href="https://es.wikipedia.org/wiki/RSS">RSS</a>
    </footer>


</body>
        </html>
    </xsl:template>
</xsl:stylesheet>